#include "main.h"
#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "IRUtils.h"
#include "motorutils.h"
#include "PIDController.h"

// global objects
Adafruit_MotorShield *AFMS;
Adafruit_DCMotor *LMotor;
Adafruit_DCMotor *RMotor;
PIDController *pid;

Fix16 fwdSpeed = FWD_INIT_SPEED;
unsigned long lastRx;
bool rec_log = false;

void setup() {
    pid = new PIDController(kp, ki, kd, maxI, maxOut);
    pid->reference = 0.0;

    Serial.begin(115200);
    AFMS = new Adafruit_MotorShield();
    LMotor = AFMS->getMotor(MOTOR_L_IDX);
    RMotor = AFMS->getMotor(MOTOR_R_IDX);
    AFMS->begin();

    Serial.println("\r\n\r\nCommands: \r\nw: Increase speed by 5%\r\ns: Decrease speed by 5%\r\n+: Increment kp by 0.01\r\n-: Decrement kp by 0.01\r\np: Toggle plot mode");
}


void loop() {
    // Go from l,r IR data to l,r motor speeds
    IRData dat = readSensors();
    Fix16 diff = dat.SENS1 - dat.SENS0;
    Fix16 pidOut = pid->update(diff);
    DriveSignal sig = curvatureDrive(fwdSpeed, pidOut);
    setMotors(sig.l, sig.r, LMotor, RMotor);

    delay(5); // Run at ~200Hz

    // Do all the user process-y serial-y stuff
    if ((Serial.available() > 0) && (millis() - lastRx > RX_TO_RX_DELAY)) {
        lastRx = millis();
        char msg[30];
        char s1[8]; 
        char s2[8];
        char byte = Serial.read();
        bool print=true;
        switch(byte) {
            case '+': 
                fix16_to_str(pid->Kp, s2, 4);
                pid->Kp += 0.01;
                fix16_to_str(pid->Kp, s1, 4);
                sprintf(msg, "Kp increased %s->%s", s2, s1);
                break;
            case '-': 
                fix16_to_str(pid->Kp, s2, 4);
                pid->Kp -= 0.01;
                fix16_to_str(pid->Kp, s1, 4);
                sprintf(msg, "Kp decreased %s->%s", s2, s1);
                break;
            case 'w': 
                fix16_to_str(fwdSpeed, s2, 4);
                fwdSpeed += 0.05; 
                if (fwdSpeed > 1.0) fwdSpeed = 1.0;
                fix16_to_str(fwdSpeed, s1, 4);
                sprintf(msg, "Speed increased %s->%s", s2, s1);
                break;
            case 's': 
                fix16_to_str(fwdSpeed, s2, 4);
                fwdSpeed -= 0.05; 
                if (fwdSpeed < 0) fwdSpeed = 0.0;
                fix16_to_str(fwdSpeed, s1, 4);
                sprintf(msg, "Speed decreased %s->%s", s2, s1);
                break;
            case 'p':
                if (rec_log)
                    sprintf(msg, "Log disabled");
                else sprintf(msg, "Log enabled\r\ntime,ir0,ir1,motorL,motorR");
                rec_log = !rec_log;
                break;
            default: print = false; break;
        }
        if (print) Serial.println(msg);
    }
    // Log if we want to
    if (rec_log) {
        char log_buf[100];
        char ir0[8];
        char ir1[8];
        char motorl[8];
        char motorr[8];
        fix16_to_str(dat.SENS0, ir0, 4);
        fix16_to_str(dat.SENS1, ir1, 4);
        fix16_to_str(sig.l, motorl, 4);
        fix16_to_str(sig.r, motorr, 4);
        sprintf(log_buf, "%ld,%s,%s,%s,%s", millis(), ir0, ir1, motorl, motorr);
        Serial.println(log_buf);
    }
}
