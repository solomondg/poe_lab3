#include "PIDController.h"
#include <Arduino.h>

PIDController::PIDController(const Fix16 &kp, const Fix16 &ki, const Fix16 &kd, const Fix16 &integralLim, const Fix16 &outputMax) {
    Kp = kp;
    Ki = ki;
    Kd = kd;
    intLimit = integralLim;
    maxOutput = outputMax;

    reset();
}

void PIDController::reset() {
    reInit = true;
}

Fix16 PIDController::update(const Fix16 &sensor) {
    Fix16 error = reference - sensor;
    if (reInit) {
        lastTimestamp = millis();
        lastError = error;
        reInit = false;
    }

    Fix16 dt = Fix16((int16_t) (millis() - lastTimestamp))/1000.0;
    lastTimestamp = millis();

    integrator += error*dt;
    if (integrator > intLimit) integrator = intLimit;
    if (integrator < intLimit*-1) integrator = intLimit*-1;

    Fix16 derivative = (error - lastError) / dt;

    Fix16 pTerm = error*Kp;
    Fix16 iTerm = integrator*Ki;
    Fix16 dTerm = derivative*Kd;

    Fix16 ret = pTerm+iTerm+dTerm;
    if (ret > maxOutput) return maxOutput;
    if (ret < maxOutput*-1) return maxOutput*-1;
    return ret;
}
