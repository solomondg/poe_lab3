# Generated by ard-make version 1.2

ARDUINO_QUIET = 1

INC_LIBS = -I/usr/share/arduino/hardware/archlinux-arduino/avr/libraries/Wire/src -I/usr/share/arduino/hardware/archlinux-arduino/avr/libraries/Wire/src/utility -Iinc -Iextern/libfixmath/inc -Iextern/motorshield/inc

BOARD_TAG = mega
BOARD_SUB = atmega2560
USER_LIB_PATH = ./lib
MONITOR_BAUDRATE = 115200
OBJDIR = ./bin
TARGET = linefollow
LOCAL_CPP_SRCS = $(wildcard src/*) $(wildcard extern/motorshield/src/*.cpp) /usr/share/arduino/hardware/archlinux-arduino/avr/libraries/Wire/src/Wire.cpp
LOCAL_C_SRCS = $(wildcard extern/libfixmath/src/*.c) /usr/share/arduino/hardware/archlinux-arduino/avr/libraries/Wire/src/utility/twi.c

CXXFLAGS_STD += $(INC_LIBS) -std=gnu++17
CFLAGS_STD += $(INC_LIBS) -std=gnu18

OPTIMIZATION_LEVEL = 0
OPTIMIZATION_LEVEL = s

include $(ARDMK_DIR)/Arduino.mk

#ARDUINO_LIB_PATH = /home/
ARDUINO_LIB_PATH = /home/solomon/Arduino/libraries
ARDUINO_LIBS = Servo Wire
