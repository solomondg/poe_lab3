#pragma once

#include "fix16.hpp"

class PIDController {
    private:
        unsigned long lastTimestamp;
        bool reInit;
        Fix16 integrator;
        Fix16 intLimit;
        Fix16 lastError;
        Fix16 maxOutput;
    public:
        Fix16 Kp, Ki, Kd;
        Fix16 reference;
        PIDController(const Fix16 &kp, const Fix16 &ki, const Fix16 &kd, const Fix16 &integralLim, const Fix16 &outputMax);
        void reset();
        Fix16 update(const Fix16 &sensor);
};
