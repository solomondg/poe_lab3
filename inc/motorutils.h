#pragma once

#include "main.h"
#include "fix16.hpp"

enum motorSide {LEFT, RIGHT};

// Ended up unused, was designed for implementing a nonlinear feedforwards term
inline static __used
Fix16 linearizeMotor(const Fix16 &speed, motorSide motor) {
    switch(motor) {
        case LEFT: return speed;
                   break;
        case RIGHT: return speed;
                    break;
        default: return speed;
    }
}

// Handle forwards/backwards motor case seamlessly, + deadband
static __used
void setMotors(
        const Fix16 &lSpeed, const Fix16 &rSpeed, 
        Adafruit_DCMotor *lMotor, Adafruit_DCMotor *rMotor
        ) {
    lMotor->run(
            lSpeed > 0.01 ? BACKWARD :
            lSpeed < -0.01 ? FORWARD :
            RELEASE
            );
    lMotor->setSpeed((int16_t) (linearizeMotor(lSpeed < -0.01 ? lSpeed*-1 : lSpeed, LEFT)*255));
    rMotor->run(
            rSpeed > 0.01 ? FORWARD :
            rSpeed < -0.01 ? BACKWARD :
            RELEASE
            );
    rMotor->setSpeed((int16_t) (linearizeMotor(rSpeed < -0.01 ? rSpeed*-1 : rSpeed, RIGHT)*255));
}

static inline __used
Fix16 fix_abs(const Fix16 &v) {
    return (v < 0.0) ? v*-1 : v;
}

struct DriveSignal {Fix16 l; Fix16 r;};

// Allow for specification of angular velocity as a proportion of linear velocity - allows for consistent controller operation at a greater range of speeds
static __used
DriveSignal curvatureDrive(const Fix16 &v, const Fix16 &w) {

    Fix16 angPower = fix_abs(v)*w;
    Fix16 right = v-angPower;
    Fix16 left = v+angPower;

    if (left > 1.0) return {1.0, right};
    if (left < -1.0) return {-1.0, right};
    if (right > 1.0) return {left, 1.0};
    if (right < -1.0) return {left, -1.0};

    return {left, right};
}
