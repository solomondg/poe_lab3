#pragma once

#include <Arduino.h>
#include "fix16.hpp"

#define __used __attribute__ ((used))
#define __pure __attribute__ ((pure))
#define __hot __attribute__ ((hot))
#define __alwaysinline __attribute__ ((always_inline))

constexpr uint8_t IR0_PORT = A0;
constexpr uint8_t IR1_PORT = A1;

constexpr uint8_t MOTOR_R_IDX = 1;
constexpr uint8_t MOTOR_L_IDX = 2;

constexpr uint8_t NUM_SENSORS = 6;

constexpr uint8_t I2C_PULLUP_SUPPLY_PIN = 13;

const Fix16 kp = 4.4;
const Fix16 ki = 0.0;
const Fix16 kd = 0.0;
const Fix16 maxI = 0.2;
const Fix16 maxOut = 1.0;

const Fix16 FWD_INIT_SPEED = 0.0;

constexpr unsigned long RX_TO_RX_DELAY = 10;
