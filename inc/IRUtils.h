#pragma once

#include <fix16.hpp>
#include <Arduino.h>
#include "main.h"

struct IRData {
    Fix16 SENS0;
    Fix16 SENS1;
};

static inline __used 
IRData readSensors() {
    return IRData {
        Fix16(analogRead(IR0_PORT))/1024.0,
            Fix16(analogRead(IR1_PORT))/1024.0
    };
}
